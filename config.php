<?php 

return [
	'db' =>[
		'server'   =>  'localhost',
		'dbname'   =>  'blog-try',
		'dbuser'   =>  'root',
		'dbpass'   =>  '',
	],

	'image_extensions'   =>  [
					'gif',
					'jpg',
					'jpeg',
					'png',
					'webp',
					'raw',
					'bmp',
	],
];
